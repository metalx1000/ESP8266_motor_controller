#include <ESP8266WiFi.h>
//needed for library
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>         //https://github.com/tzapu/WiFiManager

#include "gui.h"

int pin = 13; //D7
ESP8266WebServer server(80);


void setup() {
  WiFiManager wifiManager;
  wifiManager.autoConnect("ESP_Motor");
  Serial.begin(115200);
  delay(1000);
  pinMode(pin, OUTPUT);
  //analogWrite(pin, slider_value.toInt());
  analogWrite(pin, 0);

    // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());



  server.onNotFound(handleRoot);

  server.on("/", handleRoot);

  server.on("/update",updatemotor);

  server.begin();
  Serial.println("HTTP server started");
}

void handleRoot() {

  server.send(200, "text/html", index_html);
 
}

void updatemotor(){
   
  String message = "Updating.n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++) {
    if (server.argName(i) == "v"){
      message += "motor speed: " + server.arg(i);
      
      analogWrite(pin, server.arg(i).toInt());
    }
  }
  server.send(404, "text/plain", message);

}

void loop() {
  server.handleClient();
}
