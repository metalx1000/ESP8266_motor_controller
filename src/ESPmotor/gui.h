const char index_html[] PROGMEM = R"rawliteral(
<!DOCTYPE html>
<html lang="en">
  <head>
    <title></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
      html *{
        font-size: 35px !important;
        margin: 2px;
      }

      @media only screen and (min-width: 600px) {
        body {
          margin-right:200px;
          margin-left:200px;
          margin-top: 0;
        }
      }
      .flex{
        display: grid;
        grid-row-gap: 1rem;
        grid-template-columns: repeat(auto-fit, minmax(150px, 1fr));
      }



      .rangeslider{
        width: 100%;
      }

      .myslider {
        -webkit-appearance: none;
        background: #FF0000 ;
        width: 100%;
        height: 50px;
        opacity: 2;
      }


      .myslider::-webkit-slider-thumb {
        -webkit-appearance: none;
        cursor: pointer;
        background: #34495E ;
        width: 5%;
        height: 50px;
      }


      .myslider:hover {
        opacity: 1;
      }

    </style>
    <body>

      <h1>Motor Speed Controller</h1>
      <p>Use the slider to control the motor.</p>

      <div class="rangeslider">
        <input type="range" min="0" max="255" value="10"
        class="myslider" id="sliderRange">
        <p>Value: <span id="demo"></span></p>
      </div>

      <script>
        var rangeslider = document.getElementById("sliderRange");
        var output = document.getElementById("demo");
        output.innerHTML = rangeslider.value;

        rangeslider.oninput = function() {
          var value = this.value;
          output.innerHTML = value;
          get(`update?v=${value}`, function(data){ console.log(data); });
        }


        function get(url, success) {
          var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
          xhr.open('GET', url);
          xhr.onreadystatechange = function() {
            if (xhr.readyState>3 && xhr.status==200) success(xhr.responseText);
          };
          xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
          xhr.send();
          return xhr;
        }


      </script>

    </body>
  </html>


)rawliteral";
